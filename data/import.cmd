@echo off
call import_cmd "\COPY person (id, lastname, firstname, dob, dod, nationalities, artistname) FROM 'person.csv' DELIMITER ',' CSV HEADER QUOTE '\"' ESCAPE '''' ENCODING 'WIN1252';"
call import_cmd "\COPY artist (id, name, logo) FROM 'artist.csv' DELIMITER ',' CSV HEADER QUOTE '\"' ESCAPE '''' ENCODING 'WIN1252';"
call import_cmd "\COPY member (idartist, idperson, alias, periods) FROM 'member.csv' DELIMITER ',' CSV HEADER QUOTE '\"' ESCAPE '''' ENCODING 'WIN1252';"

call import_cmd "\COPY label (id, label, societe) FROM 'label.csv' DELIMITER ',' CSV HEADER QUOTE '\"' ESCAPE '''' ENCODING 'WIN1252';"

call import_cmd "\COPY agenda (id, date, nom, ville) FROM 'agenda.csv' DELIMITER ',' CSV HEADER QUOTE '\"' ESCAPE '''' ENCODING 'WIN1252';"

call import_cmd "\COPY song (id, title, iswc) FROM 'song.csv' DELIMITER ',' CSV HEADER QUOTE '\"' ESCAPE '''' ENCODING 'WIN1252';"
call import_cmd "\COPY recording (isrc, song, artist, length, description) FROM 'recording.csv' DELIMITER ',' CSV HEADER QUOTE '\"' ESCAPE '''' ENCODING 'WIN1252';"
call import_cmd "\COPY release (id, title, date) FROM 'release.csv' DELIMITER ',' CSV HEADER QUOTE '\"' ESCAPE '''' ENCODING 'WIN1252';"
call import_cmd "\COPY media (id, release, format, quantity) FROM 'media.csv' DELIMITER ',' CSV HEADER QUOTE '\"' ESCAPE '''' ENCODING 'WIN1252';"
call import_cmd "\COPY track (media, recording, position, number) FROM 'track.csv' DELIMITER ',' CSV HEADER QUOTE '\"' ESCAPE '''' ENCODING 'WIN1252';"
