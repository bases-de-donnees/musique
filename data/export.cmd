@echo off
call export_cmd "\copy person (id, lastname, firstname, dob, dod, nationalities, artistname) TO person.csv DELIMITER ',' CSV HEADER ENCODING 'WIN1252';"
call export_cmd "\copy artist (id, name, logo) TO artist.csv DELIMITER ',' CSV HEADER ENCODING 'WIN1252';"
call export_cmd "\copy member (idartist, idperson, alias, periods) TO member.csv DELIMITER ',' CSV HEADER ENCODING 'WIN1252';"

call export_cmd "\copy label (id, label, societe) TO label.csv DELIMITER ',' CSV HEADER ENCODING 'WIN1252';"

call export_cmd "\copy agenda (id, date, nom, ville) TO agenda.csv DELIMITER ',' CSV HEADER ENCODING 'WIN1252';"

call export_cmd "\copy song (id, title, iswc) TO song.csv DELIMITER ',' CSV HEADER ENCODING 'WIN1252';"
call export_cmd "\copy recording (isrc, song, artist, length, description) TO recording.csv DELIMITER ',' CSV HEADER ENCODING 'WIN1252';"
call export_cmd "\copy media (id, release, format, quantity) TO media.csv DELIMITER ',' CSV HEADER ENCODING 'WIN1252';"
call export_cmd "\copy release (id, title, date) TO release.csv DELIMITER ',' CSV HEADER ENCODING 'WIN1252';"
call export_cmd "\copy track (media, recording, position, number) TO track.csv DELIMITER ',' CSV HEADER ENCODING 'WIN1252';"
