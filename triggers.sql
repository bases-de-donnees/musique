-- Trigger: set_timestamp
CREATE FUNCTION trigger_set_timestamp()
  RETURNS trigger
  LANGUAGE 'plpgsql'
  COST 100
  VOLATILE NOT LEAKPROOF
AS $BODY$
BEGIN
  NEW.updatedat = NOW();
  RETURN NEW;
END;
$BODY$;

ALTER TABLE person
  ADD COLUMN "createdat" timestamp without time zone NOT NULL DEFAULT now(),
  ADD COLUMN "updatedat" timestamp without time zone;

-- DROP TRIGGER set_timestamp ON person;
CREATE TRIGGER set_timestamp
  BEFORE UPDATE
  ON person
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp();

ALTER TABLE track
  ADD COLUMN "createdat" timestamp without time zone NOT NULL DEFAULT now(),
  ADD COLUMN "updatedat" timestamp without time zone;

CREATE TRIGGER set_timestamp
  BEFORE UPDATE
  ON track
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp();

ALTER TABLE release
  ADD COLUMN "createdat" timestamp without time zone NOT NULL DEFAULT now(),
  ADD COLUMN "updatedat" timestamp without time zone;

CREATE TRIGGER set_timestamp
  BEFORE UPDATE
  ON release
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp();

ALTER TABLE recording
  ADD COLUMN "createdat" timestamp without time zone NOT NULL DEFAULT now(),
  ADD COLUMN "updatedat" timestamp without time zone;

CREATE TRIGGER set_timestamp
  BEFORE UPDATE
  ON recording
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp();

ALTER TABLE media
  ADD COLUMN "createdat" timestamp without time zone NOT NULL DEFAULT now(),
  ADD COLUMN "updatedat" timestamp without time zone;

CREATE TRIGGER set_timestamp
  BEFORE UPDATE
  ON media
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp();

ALTER TABLE song
  ADD COLUMN "createdat" timestamp without time zone NOT NULL DEFAULT now(),
  ADD COLUMN "updatedat" timestamp without time zone;

CREATE TRIGGER set_timestamp
  BEFORE UPDATE
  ON song
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp();
