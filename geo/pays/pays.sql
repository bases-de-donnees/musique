-- pays
-- --------------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS pays
(
  "code-2" character(2) NOT NULL,
  "code-3" character(3),
  "code-num" integer,
  "nom" character varying(45),
  CONSTRAINT pays_pkey PRIMARY KEY ("code-2")
);
ALTER TABLE pays OWNER to iutsd;

COMMENT ON COLUMN pays."code-2" 
  IS 'iso 3166-1 alpha 2';
COMMENT ON COLUMN pays."code-3" 
  IS 'iso 3166-1 alpha 3';
COMMENT ON COLUMN pays."code-num" 
  IS 'iso 3166-1 numeric';

copy pays ("code-2", "code-3", "code-num", "nom") 
  from 'D:\Projets\Data\Musique\geo\pays\pays.csv' 
  DELIMITER ',' CSV HEADER QUOTE '"' ESCAPE '''' ENCODING 'UTF8';
