CREATE VIEW song_without_recording AS
 SELECT s.id,
    s.title
   FROM (song s
     LEFT JOIN recording r ON ((s.id = r.song)))
  WHERE (r.isrc IS NULL);

-- View: detail
-- DROP VIEW detail;

CREATE OR REPLACE VIEW detail
 AS
 SELECT t."position",
    t.number,
    t.recording,
    (((r.length / 1000) / 60) || 'min'::text) AS duration,
    s.title,
    a.name AS artist,
    r.description,
    l.title AS album,
    m.format
   FROM (((((track t
     JOIN recording r ON ((t.recording = r.isrc)))
     JOIN song s ON ((r.song = s.id)))
     JOIN artist a ON ((r.artist = a.id)))
     JOIN media m ON ((t.media = m.id)))
     JOIN release l ON ((m.release = l.id)));

CREATE OR REPLACE VIEW doublon_song AS
 SELECT count(*) AS count,
    song.title
   FROM song
  GROUP BY song.title
 HAVING (count(*) > 1);


CREATE OR REPLACE VIEW artist_without_member AS
select artist.id, artist.name, member.idperson
FROM artist
LEFT OUTER JOIN member ON (member.idartist = artist.id)
WHERE member.idperson is null;

CREATE OR REPLACE VIEW person_without_artist AS
select person.id, person.firstname || ' ' || person.lastname, member.idperson
FROM person
LEFT OUTER JOIN member ON (member.idperson = person.id)
WHERE member.idperson is null;
