-- https://www.postgresql.org/docs/current/index.html
--

-- Dumped from database version 13.4
-- Dumped by pg_dump version 13.4

-- Started on 2021-09-25 21:54:24

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 12 (class 2615 OID 16872)
-- Name: banque; Type: SCHEMA; Schema: -; Owner: iutsd
--

CREATE SCHEMA banque;


ALTER SCHEMA banque OWNER TO iutsd;



--
-- TOC entry 13 (class 2615 OID 16874)
-- Name: flymap; Type: SCHEMA; Schema: -; Owner: iutsd
--

CREATE SCHEMA flymap;


ALTER SCHEMA flymap OWNER TO iutsd;

--
-- TOC entry 10 (class 2615 OID 16875)
-- Name: ironrace; Type: SCHEMA; Schema: -; Owner: iutsd
--

CREATE SCHEMA ironrace;


ALTER SCHEMA ironrace OWNER TO iutsd;

--
-- TOC entry 7 (class 2615 OID 16876)
-- Name: musique; Type: SCHEMA; Schema: -; Owner: iutsd
--

CREATE SCHEMA musique;


ALTER SCHEMA musique OWNER TO iutsd;

--
-- TOC entry 8 (class 2615 OID 16877)
-- Name: ski; Type: SCHEMA; Schema: -; Owner: iutsd
--

CREATE SCHEMA ski;


ALTER SCHEMA ski OWNER TO iutsd;

--
-- TOC entry 2 (class 3079 OID 16878)
-- Name: pgcrypto; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;


--
-- TOC entry 3399 (class 0 OID 0)
-- Dependencies: 2
-- Name: EXTENSION pgcrypto; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';


--
-- TOC entry 3 (class 3079 OID 16915)
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: -
--



--
-- TOC entry 738 (class 1247 OID 16927)
-- Name: vol_statut; Type: TYPE; Schema: flymap; Owner: iutsd
--

CREATE TYPE flymap.vol_statut AS ENUM (
    'programmé',
    'en vol',
    'annulé',
    'arrivé',
    'disparu',
    'accident'
);


ALTER TYPE flymap.vol_statut OWNER TO iutsd;

--
-- TOC entry 741 (class 1247 OID 16941)
-- Name: adresse; Type: TYPE; Schema: public; Owner: iutsd
--

CREATE TYPE public.adresse AS (
	ligne1 character varying(30),
	ligne2 character varying(30),
	codepostal character varying(8)
);


ALTER TYPE public.adresse OWNER TO iutsd;

--
-- TOC entry 744 (class 1247 OID 16943)
-- Name: iso-3166-a2; Type: DOMAIN; Schema: public; Owner: iutsd
--

CREATE DOMAIN public."iso-3166-a2" AS character(2)
	CONSTRAINT "2 caractères alpha A-Z" CHECK ((VALUE ~* '^[a-z]{2}$'::text));


ALTER DOMAIN public."iso-3166-a2" OWNER TO iutsd;

--
-- TOC entry 3401 (class 0 OID 0)
-- Dependencies: 744
-- Name: DOMAIN "iso-3166-a2"; Type: COMMENT; Schema: public; Owner: iutsd
--

COMMENT ON DOMAIN public."iso-3166-a2" IS 'code ISO 3166-1 alpha-2';


--
-- TOC entry 748 (class 1247 OID 16946)
-- Name: mois; Type: DOMAIN; Schema: public; Owner: iutsd
--

CREATE DOMAIN public.mois AS integer
	CONSTRAINT mois_check CHECK (((VALUE > 0) AND (VALUE < 12)));


ALTER DOMAIN public.mois OWNER TO iutsd;


--
-- TOC entry 322 (class 1255 OID 16949)
-- Name: aeroports_insert_projection(); Type: FUNCTION; Schema: flymap; Owner: iutsd
--

CREATE FUNCTION flymap.aeroports_insert_projection() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
   srid integer := nextval('aviation.aeroports_srid_seq');
BEGIN

INSERT INTO spatial_ref_sys (srid, auth_name, proj4text) VALUES (srid, CONCAT('azimuthal equidistant ', NEW.ville), CONCAT('+proj=aeqd +lat_0=', ST_Y(NEW.localisation), ' +lon_0=', ST_X(NEW.localisation), ' +x_0=0 +y_0=0 +a=6371000 +b=6371000 +units=m +no_defs') );

NEW.srid := srid;

RETURN NEW;

END
$$;


ALTER FUNCTION flymap.aeroports_insert_projection() OWNER TO iutsd;

--
-- TOC entry 323 (class 1255 OID 16950)
-- Name: aeroports_update_projection(); Type: FUNCTION; Schema: flymap; Owner: iutsd
--

CREATE FUNCTION flymap.aeroports_update_projection() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN

UPDATE spatial_ref_sys SET auth_name = CONCAT('azimuthal equidistant ', NEW.ville), proj4text = CONCAT('+proj=aeqd +lat_0=', ST_Y(NEW.localisation), ' +lon_0=', ST_X(NEW.localisation), ' +x_0=0 +y_0=0 +a=6371000 +b=6371000 +units=m +no_defs') WHERE srid = NEW.srid;

RETURN NEW;

END
$$;


ALTER FUNCTION flymap.aeroports_update_projection() OWNER TO iutsd;

--
-- TOC entry 324 (class 1255 OID 16951)
-- Name: route_grand_cercle(); Type: FUNCTION; Schema: flymap; Owner: iutsd
--

CREATE FUNCTION flymap.route_grand_cercle() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  dateline geometry;
  route geometry;
  intersec geometry;
  frac float := 0;
  aeqd_srid integer;
BEGIN

RAISE INFO 'start % %', NEW.aeroport_origine, NEW.aeroport_destination;

--DELETE FROM spatial_ref_sys WHERE srid = '99999';
--INSERT INTO spatial_ref_sys (srid, auth_name, proj4text) VALUES ('99999', 'azimuthal equidistant', (SELECT CONCAT('+proj=aeqd +lat_0=', ST_Y(localisation), ' +lon_0=', ST_X(localisation), ' +x_0=0 +y_0=0 +a=6371000 +b=6371000 +units=m +no_defs' ) FROM aviation.aeroports WHERE code_iata = NEW.aeroport_origine));

--RAISE INFO 'set srid : done';

SELECT srid FROM aviation.aeroports WHERE code_iata = NEW.aeroport_origine INTO aeqd_srid;

dateline := ST_Transform(ST_Segmentize(ST_GeomFromText('LINESTRING(180 90, 180 -90)', 4326),5), aeqd_srid);

--RAISE INFO 'dateline : %', ST_AsText(dateline);

SELECT ST_MakeLine(ST_Transform(a.localisation, aeqd_srid), ST_Transform(b.localisation, aeqd_srid))
 FROM 
   aviation.aeroports a, aviation.aeroports b 
 WHERE 
 (a.code_iata = NEW.aeroport_origine) AND
 (b.code_iata = NEW.aeroport_destination)
INTO route;

--RAISE INFO 'route : %', ST_AsText(route);

intersec := ST_GeometryN(ST_INTERSECTION(route, dateline), 1);

RAISE INFO 'intersec : %', ST_AsText(intersec);

IF intersec IS NULL THEN
	NEW.route := ST_Transform(ST_Segmentize(route, 50000), 4326);
ELSE
        frac := ST_LineLocatePoint(route, intersec);
        RAISE INFO 'frac : %', frac;
        IF frac > 0 AND  frac < 1 THEN
		NEW.route := ST_Transform(ST_Segmentize(ST_UNION(ST_Line_Substring(route,0, frac-0.01), ST_Line_Substring(route,frac+0.01, 1)),50000),4326);
	ELSE
		NEW.route := ST_Transform(ST_Segmentize(route, 50000), 4326);
	END IF;
END IF;

--RAISE INFO 'new.route : done';

RETURN NEW;

END
$$;


ALTER FUNCTION flymap.route_grand_cercle() OWNER TO iutsd;

--
-- TOC entry 325 (class 1255 OID 16952)
-- Name: toto(); Type: FUNCTION; Schema: public; Owner: iutsd
--




ALTER FUNCTION public.toto() OWNER TO iutsd;

--
-- TOC entry 326 (class 1255 OID 16953)
-- Name: trigger_set_timestamp(); Type: FUNCTION; Schema: public; Owner: iutsd
--

CREATE FUNCTION public.trigger_set_timestamp() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  NEW.updated_at = NOW();
  RETURN NEW;
END;
$$;


ALTER FUNCTION public.trigger_set_timestamp() OWNER TO iutsd;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 209 (class 1259 OID 16954)
-- Name: account; Type: TABLE; Schema: banque; Owner: iutsd
--

CREATE TABLE banque.account (
);


ALTER TABLE banque.account OWNER TO iutsd;

--
-- TOC entry 210 (class 1259 OID 16957)
-- Name: customer; Type: TABLE; Schema: banque; Owner: iutsd
--

CREATE TABLE banque.customer (
);


ALTER TABLE banque.customer OWNER TO iutsd;

--
-- TOC entry 211 (class 1259 OID 16960)
-- Name: transaction; Type: TABLE; Schema: banque; Owner: iutsd
--

CREATE TABLE banque.transaction (
);


ALTER TABLE banque.transaction OWNER TO iutsd;






--
-- TOC entry 217 (class 1259 OID 16980)
-- Name: person; Type: TABLE; Schema: public; Owner: iutsd
--



--
-- TOC entry 3403 (class 0 OID 0)
-- Dependencies: 217
-- Name: COLUMN person.dob; Type: COMMENT; Schema: public; Owner: iutsd
--

COMMENT ON COLUMN public.person.dob IS 'date of birth';


--
-- TOC entry 3404 (class 0 OID 0)
-- Dependencies: 217
-- Name: COLUMN person.dod; Type: COMMENT; Schema: public; Owner: iutsd
--

COMMENT ON COLUMN public.person.dod IS 'date of death';


--
-- TOC entry 226 (class 1259 OID 17020)
-- Name: aeroports_srid_seq; Type: SEQUENCE; Schema: flymap; Owner: iutsd
--

CREATE SEQUENCE flymap.aeroports_srid_seq
    START WITH 100000
    INCREMENT BY 1
    MINVALUE 100000
    MAXVALUE 10000000
    CACHE 1;


ALTER TABLE flymap.aeroports_srid_seq OWNER TO iutsd;

--
-- TOC entry 227 (class 1259 OID 17022)
-- Name: aircraftmodel; Type: TABLE; Schema: flymap; Owner: iutsd
--

CREATE TABLE flymap.aircraftmodel (
    code_iata character(3) NOT NULL,
    modele character varying(30),
    constructeur character(2),
    wtc character(1),
    code_icao character(4),
    code_famille character(3),
    pax integer,
    speed integer,
    ceiling integer,
    range integer,
    mtow integer,
    mlw integer
);


ALTER TABLE flymap.aircraftmodel OWNER TO iutsd;

--
-- TOC entry 3407 (class 0 OID 0)
-- Dependencies: 227
-- Name: COLUMN aircraftmodel.wtc; Type: COMMENT; Schema: flymap; Owner: iutsd
--

COMMENT ON COLUMN flymap.aircraftmodel.wtc IS 'Wake Turbulence Category';


--
-- TOC entry 3408 (class 0 OID 0)
-- Dependencies: 227
-- Name: COLUMN aircraftmodel.pax; Type: COMMENT; Schema: flymap; Owner: iutsd
--

COMMENT ON COLUMN flymap.aircraftmodel.pax IS 'Passengers : nombre de passager en configuration standard 3 classes';


--
-- TOC entry 3409 (class 0 OID 0)
-- Dependencies: 227
-- Name: COLUMN aircraftmodel.speed; Type: COMMENT; Schema: flymap; Owner: iutsd
--

COMMENT ON COLUMN flymap.aircraftmodel.speed IS 'kts (noeuds)';


--
-- TOC entry 3410 (class 0 OID 0)
-- Dependencies: 227
-- Name: COLUMN aircraftmodel.ceiling; Type: COMMENT; Schema: flymap; Owner: iutsd
--

COMMENT ON COLUMN flymap.aircraftmodel.ceiling IS 'ft (pieds)';


--
-- TOC entry 3411 (class 0 OID 0)
-- Dependencies: 227
-- Name: COLUMN aircraftmodel.range; Type: COMMENT; Schema: flymap; Owner: iutsd
--

COMMENT ON COLUMN flymap.aircraftmodel.range IS 'nautical mile (milles nautiques)';


--
-- TOC entry 3412 (class 0 OID 0)
-- Dependencies: 227
-- Name: COLUMN aircraftmodel.mtow; Type: COMMENT; Schema: flymap; Owner: iutsd
--

COMMENT ON COLUMN flymap.aircraftmodel.mtow IS 'lbs Maximum Take-Off Weight';


--
-- TOC entry 3413 (class 0 OID 0)
-- Dependencies: 227
-- Name: COLUMN aircraftmodel.mlw; Type: COMMENT; Schema: flymap; Owner: iutsd
--

COMMENT ON COLUMN flymap.aircraftmodel.mlw IS 'lbs Maximum Landing Weight';


--
-- TOC entry 228 (class 1259 OID 17025)
-- Name: appareils; Type: TABLE; Schema: flymap; Owner: iutsd
--

CREATE TABLE flymap.appareils (
    modele character(3) NOT NULL,
    msn integer NOT NULL,
    code_icao character(6) NOT NULL,
    type character varying(20),
    premier_vol date
);


ALTER TABLE flymap.appareils OWNER TO iutsd;

--
-- TOC entry 3414 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN appareils.code_icao; Type: COMMENT; Schema: flymap; Owner: iutsd
--

COMMENT ON COLUMN flymap.appareils.code_icao IS 'icao 24 code hexadécimal Automatic Dependent Surveillance (ADS)';


--
-- TOC entry 229 (class 1259 OID 17028)
-- Name: codeshare; Type: TABLE; Schema: flymap; Owner: iutsd
--

CREATE TABLE flymap.codeshare (
    vol character varying(6) NOT NULL,
    codeshare character varying(6) NOT NULL,
    operator character(2)
);


ALTER TABLE flymap.codeshare OWNER TO iutsd;

--
-- TOC entry 230 (class 1259 OID 17031)
-- Name: constructeurs; Type: TABLE; Schema: flymap; Owner: iutsd
--

CREATE TABLE flymap.constructeurs (
    code character(2) NOT NULL,
    nom character varying(30)
);


ALTER TABLE flymap.constructeurs OWNER TO iutsd;

--
-- TOC entry 232 (class 1259 OID 17037)
-- Name: flottes; Type: TABLE; Schema: flymap; Owner: iutsd
--

CREATE TABLE flymap.flottes (
    immatriculation character varying(10) NOT NULL,
    operateur character(2) NOT NULL,
    appareil character(6),
    dates daterange
);


ALTER TABLE flymap.flottes OWNER TO iutsd;

--
-- TOC entry 233 (class 1259 OID 17043)
-- Name: operator; Type: TABLE; Schema: flymap; Owner: iutsd
--

CREATE TABLE flymap.operator (
    code_iata character(2) NOT NULL,
    code_icao character(3),
    nom character varying(40),
    callsign character varying(20),
    pays public."iso-3166-a2"
);


ALTER TABLE flymap.operator OWNER TO iutsd;

--
-- TOC entry 234 (class 1259 OID 17049)
-- Name: timezone_gid_seq; Type: SEQUENCE; Schema: flymap; Owner: iutsd
--

CREATE SEQUENCE flymap.timezone_gid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE flymap.timezone_gid_seq OWNER TO iutsd;

--
-- TOC entry 231 (class 1259 OID 17034)
-- Name: vol; Type: TABLE; Schema: flymap; Owner: iutsd
--

CREATE TABLE flymap.vol (
    identification character varying(6) NOT NULL,
    appareil_immatriculation character varying(10),
    depart timestamp with time zone NOT NULL,
    arrivee timestamp with time zone,
    statut flymap.vol_statut,
    id integer NOT NULL,
    aeroport_origine character(3)
);


ALTER TABLE flymap.vol OWNER TO iutsd;

--
-- TOC entry 235 (class 1259 OID 17051)
-- Name: vol_id_seq; Type: SEQUENCE; Schema: flymap; Owner: iutsd
--

CREATE SEQUENCE flymap.vol_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE flymap.vol_id_seq OWNER TO iutsd;

--
-- TOC entry 3415 (class 0 OID 0)
-- Dependencies: 235
-- Name: vol_id_seq; Type: SEQUENCE OWNED BY; Schema: flymap; Owner: iutsd
--

ALTER SEQUENCE flymap.vol_id_seq OWNED BY flymap.vol.id;


--
-- TOC entry 236 (class 1259 OID 17053)
-- Name: agenda; Type: TABLE; Schema: musique; Owner: iutsd
--

CREATE TABLE musique.agenda (
    id integer NOT NULL,
    date date NOT NULL,
    nom character varying(255) NOT NULL,
    ville character varying(30) NOT NULL
);


ALTER TABLE musique.agenda OWNER TO iutsd;

--
-- TOC entry 237 (class 1259 OID 17056)
-- Name: artist; Type: TABLE; Schema: musique; Owner: iutsd
--

CREATE TABLE musique.artist (
    num integer NOT NULL,
    name character varying(40) NOT NULL,
    logo character varying(50) DEFAULT NULL::character varying,
    id uuid
);


ALTER TABLE musique.artist OWNER TO iutsd;

--
-- TOC entry 238 (class 1259 OID 17060)
-- Name: media; Type: TABLE; Schema: musique; Owner: iutsd
--

CREATE TABLE musique.media (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone,
    release uuid NOT NULL,
    format character varying(10),
    quantity smallint DEFAULT 1
);


ALTER TABLE musique.media OWNER TO iutsd;

--
-- TOC entry 239 (class 1259 OID 17066)
-- Name: recording; Type: TABLE; Schema: musique; Owner: iutsd
--

CREATE TABLE musique.recording (
    isrc character(15) NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone,
    song uuid NOT NULL,
    artist uuid NOT NULL,
    length bigint,
    description character varying(30)
);


ALTER TABLE musique.recording OWNER TO iutsd;

--
-- TOC entry 240 (class 1259 OID 17070)
-- Name: release; Type: TABLE; Schema: musique; Owner: iutsd
--

CREATE TABLE musique.release (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone,
    title character varying(50) NOT NULL,
    date character varying(12)
);


ALTER TABLE musique.release OWNER TO iutsd;

--
-- TOC entry 241 (class 1259 OID 17075)
-- Name: song; Type: TABLE; Schema: musique; Owner: iutsd
--

CREATE TABLE musique.song (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL,
    modified_at timestamp without time zone,
    title character varying(60) NOT NULL,
    iswc character(15)
);


ALTER TABLE musique.song OWNER TO iutsd;

--
-- TOC entry 242 (class 1259 OID 17080)
-- Name: track; Type: TABLE; Schema: musique; Owner: iutsd
--

CREATE TABLE musique.track (
    media uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    recording character(15) NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone,
    "position" integer,
    number character varying(3)
);


ALTER TABLE musique.track OWNER TO iutsd;

--
-- TOC entry 243 (class 1259 OID 17085)
-- Name: detail; Type: VIEW; Schema: musique; Owner: iutsd
--

CREATE VIEW musique.detail AS
 SELECT t."position",
    t.number,
    t.recording,
    ((((r.length / 1000) / 60) || 'min '::text) || ((r.length / 1000) % (60)::bigint)) AS duration,
    s.title,
    a.name AS artist,
    r.description,
    l.title AS album,
    m.format
   FROM (((((musique.track t
     JOIN musique.recording r ON ((t.recording = r.isrc)))
     JOIN musique.song s ON ((r.song = s.id)))
     JOIN musique.artist a ON ((r.artist = a.id)))
     JOIN musique.media m ON ((t.media = m.id)))
     JOIN musique.release l ON ((m.release = l.id)));


ALTER TABLE musique.detail OWNER TO iutsd;

--
-- TOC entry 244 (class 1259 OID 17090)
-- Name: doublon_song; Type: VIEW; Schema: musique; Owner: iutsd
--

CREATE VIEW musique.doublon_song AS
 SELECT count(*) AS count,
    song.title
   FROM musique.song
  GROUP BY song.title
 HAVING (count(*) > 1);


ALTER TABLE musique.doublon_song OWNER TO iutsd;

--
-- TOC entry 245 (class 1259 OID 17094)
-- Name: label; Type: TABLE; Schema: musique; Owner: iutsd
--

CREATE TABLE musique.label (
    id integer NOT NULL,
    label character varying(20) NOT NULL,
    societe integer
);


ALTER TABLE musique.label OWNER TO iutsd;

--
-- TOC entry 246 (class 1259 OID 17097)
-- Name: label_id_seq; Type: SEQUENCE; Schema: musique; Owner: iutsd
--

CREATE SEQUENCE musique.label_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE musique.label_id_seq OWNER TO iutsd;

--
-- TOC entry 3416 (class 0 OID 0)
-- Dependencies: 246
-- Name: label_id_seq; Type: SEQUENCE OWNED BY; Schema: musique; Owner: iutsd
--

ALTER SEQUENCE musique.label_id_seq OWNED BY musique.label.id;


--
-- TOC entry 247 (class 1259 OID 17099)
-- Name: member; Type: TABLE; Schema: musique; Owner: iutsd
--

CREATE TABLE musique.member (
    idpersonne integer NOT NULL,
    idartist integer NOT NULL,
    alias character varying(20),
    periods daterange[]
);


ALTER TABLE musique.member OWNER TO iutsd;

--
-- TOC entry 248 (class 1259 OID 17105)
-- Name: person; Type: TABLE; Schema: musique; Owner: iutsd
--

CREATE TABLE musique.person (
)
INHERITS (public.person);


ALTER TABLE musique.person OWNER TO iutsd;

--
-- TOC entry 249 (class 1259 OID 17113)
-- Name: song_without_recording; Type: VIEW; Schema: musique; Owner: iutsd
--

CREATE VIEW musique.song_without_recording AS
 SELECT s.id,
    s.title
   FROM (musique.song s
     LEFT JOIN musique.recording r ON ((s.id = r.song)))
  WHERE (r.isrc IS NULL);


ALTER TABLE musique.song_without_recording OWNER TO iutsd;

--
-- TOC entry 250 (class 1259 OID 17117)
-- Name: language; Type: TABLE; Schema: public; Owner: iutsd
--

CREATE TABLE public.language (
    code3 character(3) NOT NULL,
    code2 character(2),
    native character varying(20) NOT NULL,
    name character varying(20) NOT NULL
);


ALTER TABLE public.language OWNER TO iutsd;

--
-- TOC entry 3417 (class 0 OID 0)
-- Dependencies: 250
-- Name: TABLE language; Type: COMMENT; Schema: public; Owner: iutsd
--

COMMENT ON TABLE public.language IS 'ok';


--
-- TOC entry 3418 (class 0 OID 0)
-- Dependencies: 250
-- Name: COLUMN language.code3; Type: COMMENT; Schema: public; Owner: iutsd
--

COMMENT ON COLUMN public.language.code3 IS 'ISO 639-3';


--
-- TOC entry 3419 (class 0 OID 0)
-- Dependencies: 250
-- Name: COLUMN language.code2; Type: COMMENT; Schema: public; Owner: iutsd
--

COMMENT ON COLUMN public.language.code2 IS 'ISO 639-1';


--
-- TOC entry 251 (class 1259 OID 17120)
-- Name: pays; Type: TABLE; Schema: public; Owner: iutsd
--

CREATE TABLE public.pays (
    "code-alpha-3" character(3) NOT NULL,
    m49 character(3) NOT NULL,
    drapeau character(2) NOT NULL,
    nom character varying(60) NOT NULL,
    "abréviation" character varying(5) NOT NULL,
    "code-alpha-2" public."iso-3166-a2" NOT NULL
);


ALTER TABLE public.pays OWNER TO iutsd;

--
-- TOC entry 3420 (class 0 OID 0)
-- Dependencies: 251
-- Name: COLUMN pays."code-alpha-3"; Type: COMMENT; Schema: public; Owner: iutsd
--

COMMENT ON COLUMN public.pays."code-alpha-3" IS 'ISO 3166 alpha-3';


--
-- TOC entry 3421 (class 0 OID 0)
-- Dependencies: 251
-- Name: COLUMN pays.m49; Type: COMMENT; Schema: public; Owner: iutsd
--

COMMENT ON COLUMN public.pays.m49 IS 'ISO 3166 numeric-3 / UN M.49';


--
-- TOC entry 252 (class 1259 OID 17126)
-- Name: personne; Type: TABLE; Schema: public; Owner: iutsd
--

CREATE TABLE public.personne (
    nom character varying(30) NOT NULL,
    prenom character varying(30) NOT NULL,
    genre bit(1) NOT NULL,
    naissance date NOT NULL,
    id integer NOT NULL,
    "nationalités" character(2)[],
    deces date
);


ALTER TABLE public.personne OWNER TO iutsd;

--
-- TOC entry 253 (class 1259 OID 17132)
-- Name: personne_id_seq; Type: SEQUENCE; Schema: public; Owner: iutsd
--

CREATE SEQUENCE public.personne_id_seq
    START WITH 100001
    INCREMENT BY 1
    MINVALUE 100001
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.personne_id_seq OWNER TO iutsd;

--
-- TOC entry 3422 (class 0 OID 0)
-- Dependencies: 253
-- Name: personne_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: iutsd
--

ALTER SEQUENCE public.personne_id_seq OWNED BY public.personne.id;


--
-- TOC entry 254 (class 1259 OID 17134)
-- Name: prenom; Type: TABLE; Schema: public; Owner: iutsd
--

CREATE TABLE public.prenom (
    sexe smallint NOT NULL,
    prenom character varying(20) NOT NULL,
    annee smallint NOT NULL,
    departement smallint NOT NULL,
    nombre integer,
    cumulative double precision DEFAULT 0.0
);


ALTER TABLE public.prenom OWNER TO iutsd;

--
-- TOC entry 255 (class 1259 OID 17138)
-- Name: pyramide; Type: TABLE; Schema: public; Owner: iutsd
--

CREATE TABLE public.pyramide (
    annee integer NOT NULL,
    sexe smallint NOT NULL,
    nombre bigint NOT NULL,
    cumulative real
);


ALTER TABLE public.pyramide OWNER TO iutsd;

--
-- TOC entry 3423 (class 0 OID 0)
-- Dependencies: 255
-- Name: TABLE pyramide; Type: COMMENT; Schema: public; Owner: iutsd
--

COMMENT ON TABLE public.pyramide IS 'Données annuelles 2021';


--
-- TOC entry 256 (class 1259 OID 17141)
-- Name: société; Type: TABLE; Schema: public; Owner: iutsd
--

CREATE TABLE public."société" (
    id integer NOT NULL,
    nom character varying(39) NOT NULL,
    "siège" public.adresse,
    active daterange,
    "dénomination" character varying(39),
    "activité" character varying(6),
    "identité" character varying(19)
);


ALTER TABLE public."société" OWNER TO iutsd;

--
-- TOC entry 257 (class 1259 OID 17147)
-- Name: société_id_seq; Type: SEQUENCE; Schema: public; Owner: iutsd
--

CREATE SEQUENCE public."société_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."société_id_seq" OWNER TO iutsd;

--
-- TOC entry 3424 (class 0 OID 0)
-- Dependencies: 257
-- Name: société_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: iutsd
--

ALTER SEQUENCE public."société_id_seq" OWNED BY public."société".id;


--
-- TOC entry 258 (class 1259 OID 17149)
-- Name: tutorials; Type: TABLE; Schema: public; Owner: iutsd
--

CREATE TABLE public.tutorials (
    id integer NOT NULL,
    title character varying(255),
    description character varying(255),
    published boolean,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public.tutorials OWNER TO iutsd;

--
-- TOC entry 259 (class 1259 OID 17155)
-- Name: tutorials_id_seq; Type: SEQUENCE; Schema: public; Owner: iutsd
--

CREATE SEQUENCE public.tutorials_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tutorials_id_seq OWNER TO iutsd;

--
-- TOC entry 3425 (class 0 OID 0)
-- Dependencies: 259
-- Name: tutorials_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: iutsd
--

ALTER SEQUENCE public.tutorials_id_seq OWNED BY public.tutorials.id;


--
-- TOC entry 260 (class 1259 OID 17157)
-- Name: user; Type: TABLE; Schema: public; Owner: iutsd
--

CREATE TABLE public."user" (
    id uuid NOT NULL,
    nom character varying(20),
    "prénom" character varying,
    genre smallint,
    naissance date,
    pays public."iso-3166-a2"
);


ALTER TABLE public."user" OWNER TO iutsd;

--
-- TOC entry 261 (class 1259 OID 17163)
-- Name: ville_id_seq; Type: SEQUENCE; Schema: public; Owner: iutsd
--

CREATE SEQUENCE public.ville_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ville_id_seq OWNER TO iutsd;

--
-- TOC entry 262 (class 1259 OID 17165)
-- Name: stations; Type: TABLE; Schema: ski; Owner: iutsd
--

CREATE TABLE ski.stations (
);


ALTER TABLE ski.stations OWNER TO iutsd;

--
-- TOC entry 3138 (class 2604 OID 17173)
-- Name: vol id; Type: DEFAULT; Schema: flymap; Owner: iutsd
--

ALTER TABLE ONLY flymap.vol ALTER COLUMN id SET DEFAULT nextval('flymap.vol_id_seq'::regclass);


--
-- TOC entry 3150 (class 2604 OID 17174)
-- Name: label id; Type: DEFAULT; Schema: musique; Owner: iutsd
--

ALTER TABLE ONLY musique.label ALTER COLUMN id SET DEFAULT nextval('musique.label_id_seq'::regclass);


--
-- TOC entry 3151 (class 2604 OID 17175)
-- Name: person id; Type: DEFAULT; Schema: musique; Owner: iutsd
--

ALTER TABLE ONLY musique.person ALTER COLUMN id SET DEFAULT public.uuid_generate_v4();


--
-- TOC entry 3152 (class 2604 OID 17176)
-- Name: person created_at; Type: DEFAULT; Schema: musique; Owner: iutsd
--

ALTER TABLE ONLY musique.person ALTER COLUMN created_at SET DEFAULT now();


--
-- TOC entry 3153 (class 2604 OID 17177)
-- Name: personne id; Type: DEFAULT; Schema: public; Owner: iutsd
--

ALTER TABLE ONLY public.personne ALTER COLUMN id SET DEFAULT nextval('public.personne_id_seq'::regclass);


--
-- TOC entry 3155 (class 2604 OID 17178)
-- Name: société id; Type: DEFAULT; Schema: public; Owner: iutsd
--

ALTER TABLE ONLY public."société" ALTER COLUMN id SET DEFAULT nextval('public."société_id_seq"'::regclass);


--
-- TOC entry 3156 (class 2604 OID 17179)
-- Name: tutorials id; Type: DEFAULT; Schema: public; Owner: iutsd
--

ALTER TABLE ONLY public.tutorials ALTER COLUMN id SET DEFAULT nextval('public.tutorials_id_seq'::regclass);

--
-- TOC entry 3182 (class 2606 OID 17201)
-- Name: appareils appareils_pkey; Type: CONSTRAINT; Schema: flymap; Owner: iutsd
--

ALTER TABLE ONLY flymap.appareils
    ADD CONSTRAINT appareils_pkey PRIMARY KEY (code_icao);


--
-- TOC entry 3185 (class 2606 OID 17203)
-- Name: codeshare codeshares_pkey; Type: CONSTRAINT; Schema: flymap; Owner: iutsd
--

ALTER TABLE ONLY flymap.codeshare
    ADD CONSTRAINT codeshares_pkey PRIMARY KEY (codeshare);


--
-- TOC entry 3195 (class 2606 OID 17205)
-- Name: operator compagnies_pkey; Type: CONSTRAINT; Schema: flymap; Owner: iutsd
--

ALTER TABLE ONLY flymap.operator
    ADD CONSTRAINT compagnies_pkey PRIMARY KEY (code_iata);


--
-- TOC entry 3188 (class 2606 OID 17207)
-- Name: constructeurs constructeurs_pkey; Type: CONSTRAINT; Schema: flymap; Owner: iutsd
--

ALTER TABLE ONLY flymap.constructeurs
    ADD CONSTRAINT constructeurs_pkey PRIMARY KEY (code);


--
-- TOC entry 3193 (class 2606 OID 17209)
-- Name: flottes flottes_pkey; Type: CONSTRAINT; Schema: flymap; Owner: iutsd
--

ALTER TABLE ONLY flymap.flottes
    ADD CONSTRAINT flottes_pkey PRIMARY KEY (operateur, immatriculation);


--
-- TOC entry 3180 (class 2606 OID 17211)
-- Name: aircraftmodel modeles_pkey; Type: CONSTRAINT; Schema: flymap; Owner: iutsd
--

ALTER TABLE ONLY flymap.aircraftmodel
    ADD CONSTRAINT modeles_pkey PRIMARY KEY (code_iata);


--
-- TOC entry 3191 (class 2606 OID 17213)
-- Name: vol vol_pkey; Type: CONSTRAINT; Schema: flymap; Owner: iutsd
--

ALTER TABLE ONLY flymap.vol
    ADD CONSTRAINT vol_pkey PRIMARY KEY (id);


--
-- TOC entry 3214 (class 2606 OID 17215)
-- Name: label label_pkey; Type: CONSTRAINT; Schema: musique; Owner: iutsd
--

ALTER TABLE ONLY musique.label
    ADD CONSTRAINT label_pkey PRIMARY KEY (id);


--
-- TOC entry 3216 (class 2606 OID 17217)
-- Name: member member_pkey; Type: CONSTRAINT; Schema: musique; Owner: iutsd
--

ALTER TABLE ONLY musique.member
    ADD CONSTRAINT member_pkey PRIMARY KEY (idpersonne, idartist);


--
-- TOC entry 3201 (class 2606 OID 17219)
-- Name: media pk_media; Type: CONSTRAINT; Schema: musique; Owner: iutsd
--

ALTER TABLE ONLY musique.media
    ADD CONSTRAINT pk_media PRIMARY KEY (id);


--
-- TOC entry 3204 (class 2606 OID 17221)
-- Name: recording pk_recording; Type: CONSTRAINT; Schema: musique; Owner: iutsd
--

ALTER TABLE ONLY musique.recording
    ADD CONSTRAINT pk_recording PRIMARY KEY (isrc);


--
-- TOC entry 3206 (class 2606 OID 17223)
-- Name: release pk_release; Type: CONSTRAINT; Schema: musique; Owner: iutsd
--

ALTER TABLE ONLY musique.release
    ADD CONSTRAINT pk_release PRIMARY KEY (id);


--
-- TOC entry 3208 (class 2606 OID 17225)
-- Name: song pk_song; Type: CONSTRAINT; Schema: musique; Owner: iutsd
--

ALTER TABLE ONLY musique.song
    ADD CONSTRAINT pk_song PRIMARY KEY (id);


--
-- TOC entry 3198 (class 2606 OID 17227)
-- Name: artist pk_vinylbase_artist; Type: CONSTRAINT; Schema: musique; Owner: iutsd
--

ALTER TABLE ONLY musique.artist
    ADD CONSTRAINT pk_vinylbase_artist PRIMARY KEY (num);


--
-- TOC entry 3212 (class 2606 OID 17229)
-- Name: track track_pkey; Type: CONSTRAINT; Schema: musique; Owner: iutsd
--

ALTER TABLE ONLY musique.track
    ADD CONSTRAINT track_pkey PRIMARY KEY (media, recording);


--
-- TOC entry 3220 (class 2606 OID 17231)
-- Name: pays pays_code-alpha-3; Type: CONSTRAINT; Schema: public; Owner: iutsd
--

ALTER TABLE ONLY public.pays
    ADD CONSTRAINT "pays_code-alpha-3" UNIQUE ("code-alpha-3");


--
-- TOC entry 3222 (class 2606 OID 17233)
-- Name: pays pays_m49; Type: CONSTRAINT; Schema: public; Owner: iutsd
--

ALTER TABLE ONLY public.pays
    ADD CONSTRAINT pays_m49 UNIQUE (m49);


--
-- TOC entry 3225 (class 2606 OID 17235)
-- Name: pays pays_pkey; Type: CONSTRAINT; Schema: public; Owner: iutsd
--

ALTER TABLE ONLY public.pays
    ADD CONSTRAINT pays_pkey PRIMARY KEY ("code-alpha-2");


--
-- TOC entry 3164 (class 2606 OID 17237)
-- Name: person person_fk; Type: CONSTRAINT; Schema: public; Owner: iutsd
--

ALTER TABLE ONLY public.person
    ADD CONSTRAINT person_fk PRIMARY KEY (id);


--
-- TOC entry 3227 (class 2606 OID 17239)
-- Name: personne personnes_id; Type: CONSTRAINT; Schema: public; Owner: iutsd
--

ALTER TABLE ONLY public.personne
    ADD CONSTRAINT personnes_id PRIMARY KEY (id);


--
-- TOC entry 3237 (class 2606 OID 17241)
-- Name: user personnes_pkey; Type: CONSTRAINT; Schema: public; Owner: iutsd
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT personnes_pkey PRIMARY KEY (id);


--
-- TOC entry 3218 (class 2606 OID 17243)
-- Name: language pk_language; Type: CONSTRAINT; Schema: public; Owner: iutsd
--

ALTER TABLE ONLY public.language
    ADD CONSTRAINT pk_language PRIMARY KEY (code3);


--
-- TOC entry 3229 (class 2606 OID 17245)
-- Name: prenom prenom_pkey; Type: CONSTRAINT; Schema: public; Owner: iutsd
--

ALTER TABLE ONLY public.prenom
    ADD CONSTRAINT prenom_pkey PRIMARY KEY (sexe, prenom, annee, departement);


--
-- TOC entry 3231 (class 2606 OID 17254)
-- Name: pyramide pyramide_pkey; Type: CONSTRAINT; Schema: public; Owner: iutsd
--

ALTER TABLE ONLY public.pyramide
    ADD CONSTRAINT pyramide_pkey PRIMARY KEY (annee, sexe);


--
-- TOC entry 3233 (class 2606 OID 17256)
-- Name: société société_pkey; Type: CONSTRAINT; Schema: public; Owner: iutsd
--

ALTER TABLE ONLY public."société"
    ADD CONSTRAINT "société_pkey" PRIMARY KEY (id);


--
-- TOC entry 3235 (class 2606 OID 17258)
-- Name: tutorials tutorials_pkey; Type: CONSTRAINT; Schema: public; Owner: iutsd
--

ALTER TABLE ONLY public.tutorials
    ADD CONSTRAINT tutorials_pkey PRIMARY KEY (id);


--
-- TOC entry 3183 (class 1259 OID 17265)
-- Name: codeshares_operateur_idx; Type: INDEX; Schema: flymap; Owner: iutsd
--

CREATE INDEX codeshares_operateur_idx ON flymap.codeshare USING btree (operator);


--
-- TOC entry 3186 (class 1259 OID 17266)
-- Name: codeshares_vol_idx; Type: INDEX; Schema: flymap; Owner: iutsd
--

CREATE INDEX codeshares_vol_idx ON flymap.codeshare USING btree (vol);


--
-- TOC entry 3178 (class 1259 OID 17267)
-- Name: fki_constructeurs; Type: INDEX; Schema: flymap; Owner: iutsd
--

CREATE INDEX fki_constructeurs ON flymap.aircraftmodel USING btree (constructeur);


--
-- TOC entry 3189 (class 1259 OID 17268)
-- Name: fki_vol_route_fk; Type: INDEX; Schema: flymap; Owner: iutsd
--

CREATE INDEX fki_vol_route_fk ON flymap.vol USING btree (identification, aeroport_origine);


--
-- TOC entry 3196 (class 1259 OID 17269)
-- Name: operateurs_code_pays_idx; Type: INDEX; Schema: flymap; Owner: iutsd
--

CREATE INDEX operateurs_code_pays_idx ON flymap.operator USING btree (pays);


--
-- TOC entry 3199 (class 1259 OID 17270)
-- Name: fki_fk_media_release; Type: INDEX; Schema: musique; Owner: iutsd
--

CREATE INDEX fki_fk_media_release ON musique.media USING btree (release);


--
-- TOC entry 3202 (class 1259 OID 17271)
-- Name: fki_fk_recording_song; Type: INDEX; Schema: musique; Owner: iutsd
--

CREATE INDEX fki_fk_recording_song ON musique.recording USING btree (song);


--
-- TOC entry 3209 (class 1259 OID 17272)
-- Name: fki_fk_track_media; Type: INDEX; Schema: musique; Owner: iutsd
--

CREATE INDEX fki_fk_track_media ON musique.track USING btree (media);


--
-- TOC entry 3210 (class 1259 OID 17273)
-- Name: fki_fk_track_recording; Type: INDEX; Schema: musique; Owner: iutsd
--

CREATE INDEX fki_fk_track_recording ON musique.track USING btree (recording);


--
-- TOC entry 3223 (class 1259 OID 17274)
-- Name: pays_nom; Type: INDEX; Schema: public; Owner: iutsd
--

CREATE INDEX pays_nom ON public.pays USING btree (nom);


--
-- TOC entry 3165 (class 1259 OID 17275)
-- Name: person_lastname_indx; Type: INDEX; Schema: public; Owner: iutsd
--

CREATE INDEX person_lastname_indx ON public.person USING btree (lastname);



--
-- TOC entry 3253 (class 2620 OID 17277)
-- Name: media set_timestamp; Type: TRIGGER; Schema: musique; Owner: iutsd
--

CREATE TRIGGER set_timestamp BEFORE UPDATE ON musique.media FOR EACH ROW EXECUTE FUNCTION public.trigger_set_timestamp();


--
-- TOC entry 3254 (class 2620 OID 17278)
-- Name: recording set_timestamp; Type: TRIGGER; Schema: musique; Owner: iutsd
--

CREATE TRIGGER set_timestamp BEFORE UPDATE ON musique.recording FOR EACH ROW EXECUTE FUNCTION public.trigger_set_timestamp();


--
-- TOC entry 3255 (class 2620 OID 17279)
-- Name: release set_timestamp; Type: TRIGGER; Schema: musique; Owner: iutsd
--

CREATE TRIGGER set_timestamp BEFORE UPDATE ON musique.release FOR EACH ROW EXECUTE FUNCTION public.trigger_set_timestamp();


--
-- TOC entry 3256 (class 2620 OID 17280)
-- Name: track set_timestamp; Type: TRIGGER; Schema: musique; Owner: iutsd
--

CREATE TRIGGER set_timestamp BEFORE UPDATE ON musique.track FOR EACH ROW EXECUTE FUNCTION public.trigger_set_timestamp();


--
-- TOC entry 3251 (class 2620 OID 17281)
-- Name: person set_timestamp; Type: TRIGGER; Schema: public; Owner: iutsd
--

CREATE TRIGGER set_timestamp BEFORE UPDATE ON public.person FOR EACH ROW EXECUTE FUNCTION public.trigger_set_timestamp();



--
-- TOC entry 3243 (class 2606 OID 17307)
-- Name: codeshare fk_codeshare_operator; Type: FK CONSTRAINT; Schema: flymap; Owner: iutsd
--

ALTER TABLE ONLY flymap.codeshare
    ADD CONSTRAINT fk_codeshare_operator FOREIGN KEY (operator) REFERENCES flymap.operator(code_iata) NOT VALID;


--
-- TOC entry 3244 (class 2606 OID 17312)
-- Name: operator fk_operator_pays; Type: FK CONSTRAINT; Schema: flymap; Owner: iutsd
--

ALTER TABLE ONLY flymap.operator
    ADD CONSTRAINT fk_operator_pays FOREIGN KEY (pays) REFERENCES public.pays("code-alpha-2") NOT VALID;


--
-- TOC entry 3245 (class 2606 OID 17317)
-- Name: media fk_media_release; Type: FK CONSTRAINT; Schema: musique; Owner: iutsd
--

ALTER TABLE ONLY musique.media
    ADD CONSTRAINT fk_media_release FOREIGN KEY (release) REFERENCES musique.release(id) NOT VALID;


--
-- TOC entry 3246 (class 2606 OID 17322)
-- Name: recording fk_recording_song; Type: FK CONSTRAINT; Schema: musique; Owner: iutsd
--

ALTER TABLE ONLY musique.recording
    ADD CONSTRAINT fk_recording_song FOREIGN KEY (song) REFERENCES musique.song(id) NOT VALID;


--
-- TOC entry 3247 (class 2606 OID 17327)
-- Name: track fk_track_recording; Type: FK CONSTRAINT; Schema: musique; Owner: iutsd
--

ALTER TABLE ONLY musique.track
    ADD CONSTRAINT fk_track_recording FOREIGN KEY (recording) REFERENCES musique.recording(isrc) ON DELETE CASCADE;


--
-- TOC entry 3248 (class 2606 OID 17332)
-- Name: member member_idartist_fkey; Type: FK CONSTRAINT; Schema: musique; Owner: iutsd
--

ALTER TABLE ONLY musique.member
    ADD CONSTRAINT member_idartist_fkey FOREIGN KEY (idartist) REFERENCES musique.artist(num) NOT VALID;


--
-- TOC entry 3249 (class 2606 OID 17337)
-- Name: member member_idpersonne_fkey; Type: FK CONSTRAINT; Schema: musique; Owner: iutsd
--

ALTER TABLE ONLY musique.member
    ADD CONSTRAINT member_idpersonne_fkey FOREIGN KEY (idpersonne) REFERENCES public.personne(id) NOT VALID;


--
-- TOC entry 3250 (class 2606 OID 17342)
-- Name: user fk_utilisateur_pays; Type: FK CONSTRAINT; Schema: public; Owner: iutsd
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT fk_utilisateur_pays FOREIGN KEY (pays) REFERENCES public.pays("code-alpha-2") NOT VALID;


-- Completed on 2021-09-25 21:54:26

--
-- PostgreSQL database dump complete
--
