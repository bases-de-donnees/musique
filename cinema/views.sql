CREATE VIEW cinema.movie_without_castandcrew AS
 SELECT m.id,
    m.title
   FROM (cinema.movie m
     LEFT JOIN cinema.castandcrew c ON ((m.id = c.movie)))
  WHERE (c.id IS NULL);


ALTER TABLE cinema.movie_without_castandcrew OWNER TO iutsd;

--
-- TOC entry 216 (class 1259 OID 16976)
-- Name: movie_without_director; Type: VIEW; Schema: cinema; Owner: iutsd
--

CREATE VIEW cinema.movie_without_director AS
 SELECT m.id,
    m.title
   FROM (cinema.movie m
     LEFT JOIN cinema.castandcrew c ON (((m.id = c.movie) AND ((c.role)::text = 'director'::text))))
  WHERE (c.id IS NULL);
ALTER TABLE cinema.movie_without_director OWNER TO iutsd;