-- 
CREATE DATABASE cinema
  WITH OWNER = iutsd
  TEMPLATE = template0
  ENCODING = 'UTF8'
  LC_COLLATE = 'fr_FR.UTF-8'
  LC_CTYPE = 'fr_FR.UTF-8'
  TABLESPACE = pg_default
  CONNECTION LIMIT = -1;
