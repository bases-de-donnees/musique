
--SET search_path TO cinema,public;

-- personnes
-- --------------------------------------------------------------------------------
CREATE TABLE person (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    lastname character varying(30) NOT NULL,
    firstname character varying(30),
    dob date NOT NULL,
    dod date,
    nationalities character(2)[],
    artistname character varying(30),
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone
);
ALTER TABLE ONLY person ADD CONSTRAINT person_pk PRIMARY KEY (id);
ALTER TABLE person OWNER TO iutsd;
-- --------------------------------------------------------------------------------

-- films
-- --------------------------------------------------------------------------------
CREATE TABLE movie (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    title character varying(80) NOT NULL,
    title_original character varying(80),
    year integer,
    release date,
    runtime integer
);
ALTER TABLE ONLY movie ADD CONSTRAINT movie_pk PRIMARY KEY (id);
ALTER TABLE movie OWNER TO iutsd;
-- --------------------------------------------------------------------------------



-- --------------------------------------------------------------------------------
CREATE TABLE castandcrew (
    id integer NOT NULL,
    movie uuid NOT NULL,
    person uuid NOT NULL,
    role character varying(25),
    alias character varying(40)
);

ALTER TABLE castandcrew OWNER TO iutsd;

CREATE SEQUENCE castandcrew_id_seq
  AS integer
  START WITH 1
  INCREMENT BY 1
  NO MINVALUE
  NO MAXVALUE
  CACHE 1;

ALTER TABLE castandcrew_id_seq OWNER TO iutsd;
ALTER SEQUENCE castandcrew_id_seq OWNED BY castandcrew.id;
-- --------------------------------------------------------------------------------

-- --------------------------------------------------------------------------------
CREATE TYPE adresse AS
(
	voie character varying(40),
	codepostal character varying(10),
	ville character varying(30)
);

ALTER TYPE adresse OWNER TO iutsd;
-- --------------------------------------------------------------------------------

-- tickets
-- --------------------------------------------------------------------------------
CREATE TABLE ticket (
  id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
  showtime integer,
  price numeric(4,2),
  created_at timestamp without time zone DEFAULT now(),
  serial integer NOT NULL,
  signature character varying(80)
);
ALTER TABLE ONLY ticket ADD CONSTRAINT ticket_pk PRIMARY KEY (id);
ALTER TABLE ticket OWNER TO iutsd;

CREATE SEQUENCE ticket_serial_seq
  AS integer
  START WITH 1
  INCREMENT BY 1
  NO MINVALUE
  NO MAXVALUE
  CACHE 1;

ALTER TABLE ONLY ticket ALTER COLUMN serial SET DEFAULT nextval('ticket_serial_seq'::regclass);
ALTER SEQUENCE ticket_serial_seq OWNED BY ticket.serial;
ALTER TABLE ticket_serial_seq OWNER TO iutsd;
-- --------------------------------------------------------------------------------
