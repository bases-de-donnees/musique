@echo off

"C:\\Program Files\\pgAdmin 4\\runtime\\psql.exe" ^
--host iutsd-raspberry30-896.ad.univ-lorraine.fr ^
--dbname postgres ^
--username postgres ^
--file database.sql

call musique_cmd extensions.sql
call musique_cmd tables.sql
call musique_cmd constrains.sql
call musique_cmd triggers.sql
call musique_cmd views.sql
call musique_cmd owner.sql

cd data
call import.cmd

cd ..
