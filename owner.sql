alter table "person" owner to musique;
alter table "artist" owner to musique;
alter table "member" owner to musique;

alter table "label" owner to musique;

alter table "song" owner to musique;
alter table "media" owner to musique;
alter table "recording" owner to musique;
alter table "release" owner to musique;
alter table "track" owner to musique;
alter table "label" owner to musique;
ALTER TABLE "agenda" OWNER TO musique;

alter table "song_without_recording" owner to musique;
alter table "detail" owner to musique;
alter table "doublon_song" owner to musique;

ALTER TABLE label_id_seq OWNER TO musique;
