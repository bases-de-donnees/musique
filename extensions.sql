CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';

CREATE EXTENSION IF NOT EXISTS "postgis";

CREATE TYPE adresse AS (
	voie character varying(40),
	codepostal character varying(10),
	ville character varying(30)
);
ALTER TYPE adresse OWNER TO iutsd;
