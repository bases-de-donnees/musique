-- Table: person
CREATE TABLE person
(
  id uuid NOT NULL DEFAULT uuid_generate_v4(),
  lastname character varying(30) COLLATE pg_catalog."default" NOT NULL,
  firstname character varying(30) COLLATE pg_catalog."default",
  dob date NOT NULL,
  dod date,
  nationalities character(2)[] COLLATE pg_catalog."default",
  artistname character varying(30) COLLATE pg_catalog."default",
  CONSTRAINT person_fkey PRIMARY KEY (id)
);

COMMENT ON COLUMN person.dob IS 'date of birth';
COMMENT ON COLUMN person.dod IS 'date of death';

-- Index: person_lastname_indx
-- DROP INDEX person_lastname_indx;

CREATE INDEX person_lastname_indx
  ON person USING btree
  (lastname COLLATE pg_catalog."default" ASC NULLS LAST);

-- Table: artist
CREATE TABLE artist
(
  id uuid NOT NULL DEFAULT uuid_generate_v4(),
  name character varying(40) COLLATE pg_catalog."default" NOT NULL,
  logo character varying(50) COLLATE pg_catalog."default",
  CONSTRAINT artist_pkey PRIMARY KEY (id)
);

-- Table: member
CREATE TABLE member
(
  idperson uuid NOT NULL,
  idartist uuid NOT NULL,
  alias character varying(20) COLLATE pg_catalog."default",
  periods daterange[],
  CONSTRAINT member_pkey PRIMARY KEY (idperson, idartist)
);

-- Table: media
CREATE TABLE media
(
  id uuid NOT NULL DEFAULT uuid_generate_v4(),
  release uuid NOT NULL,
  format character varying(10) COLLATE pg_catalog."default",
  quantity smallint DEFAULT 1,
  CONSTRAINT media_pkey PRIMARY KEY (id)
);

-- Index: fki_fk_media_release
-- DROP INDEX fki_fk_media_release;

CREATE INDEX fki_fk_media_release ON media USING btree
  (release ASC NULLS LAST);


-- Table: track
CREATE TABLE track
(
  media uuid NOT NULL DEFAULT uuid_generate_v4(),
  recording character(15) COLLATE pg_catalog."default" NOT NULL,
  "position" integer,
  "number" character varying(5) COLLATE pg_catalog."default",
  CONSTRAINT track_pkey PRIMARY KEY (media, recording)
);

-- Index: fki_fk_track_media
-- DROP INDEX fki_fk_track_media;

CREATE INDEX fki_fk_track_media
  ON track USING btree
  (media ASC NULLS LAST);

-- Index: fki_fk_track_recording
-- DROP INDEX fki_fk_track_recording;

CREATE INDEX fki_fk_track_recording
  ON track USING btree
  (recording COLLATE pg_catalog."default" ASC NULLS LAST);

-- Table: song
CREATE TABLE song
(
  id uuid NOT NULL DEFAULT uuid_generate_v4(),
  title character varying(60) COLLATE pg_catalog."default" NOT NULL,
  iswc character(15) COLLATE pg_catalog."default",
  CONSTRAINT pk_song PRIMARY KEY (id)
);

-- Table: release
CREATE TABLE release
(
  id uuid NOT NULL DEFAULT uuid_generate_v4(),
  title character varying(50) COLLATE pg_catalog."default" NOT NULL,
  date character varying(12) COLLATE pg_catalog."default",
  CONSTRAINT release_pkey PRIMARY KEY (id)
);

-- Table: recording
CREATE TABLE recording
(
  isrc character(15) COLLATE pg_catalog."default" NOT NULL,
  song uuid NOT NULL,
  artist uuid NOT NULL,
  length bigint,
  description character varying(30) COLLATE pg_catalog."default",
  CONSTRAINT pk_recording PRIMARY KEY (isrc)
);

-- DROP INDEX fki_fk_recording_song
-- Index: fki_fk_recording_song

CREATE INDEX fki_fk_recording_song
  ON recording USING btree
  (song ASC NULLS LAST);


CREATE TABLE agenda (
    id integer NOT NULL,
    date date NOT NULL,
    nom character varying(255) NOT NULL,
    ville character varying(30) NOT NULL
);

-- Table: label
CREATE TABLE label
(
  id integer NOT NULL,
  label character varying(20) COLLATE pg_catalog."default" NOT NULL,
  "societe" integer,
  CONSTRAINT label_pkey PRIMARY KEY (id)
);

CREATE SEQUENCE label_id_seq
  AS integer
  START WITH 1
  INCREMENT BY 1
  NO MINVALUE
  NO MAXVALUE
  CACHE 1;

ALTER SEQUENCE label_id_seq OWNED BY label.id;
ALTER TABLE ONLY label ALTER COLUMN id SET DEFAULT nextval('label_id_seq'::regclass);
